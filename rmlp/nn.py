import pickle
from typing import Optional

import numpy as np

from rmlp.types import Errors

NN: type = tuple[np.ndarray, np.ndarray, np.ndarray]


def make_layer(
        inputs_count: int,
        outputs_count: int,
        low: float = 0.01,
        high: float = 0.02,
        scale: float = 1.0,
        loop: bool = True) -> np.ndarray:
    if loop:
        return np.random.uniform(
            low=low * scale,
            high=high * scale,
            size=(outputs_count, inputs_count + outputs_count + 1))
    return np.random.uniform(
        low=low * scale,
        high=high * scale,
        size=(outputs_count, inputs_count + 1))


def make_nn(
        inputs_count: int,
        h1_count: int,
        h2_count: int,
        outputs_count: int) -> NN:
    l1 = make_layer(inputs_count, h1_count, scale=0.1)
    l2 = make_layer(h1_count, h2_count)
    l3 = make_layer(h2_count, outputs_count, loop=False, scale=0.1)

    return l1, l2, l3


def save_nn(file: str, nn: NN) -> None:
    print(f'saving nn to "{file}"...')
    with open(file, 'wb') as nn_file:
        pickle.dump(nn, nn_file)


def load_nn(file: str) -> NN:
    print(f'loading nn from "{file}"...')
    with open(file, 'rb') as nn_file:
        return pickle.load(nn_file)


def save_nn_and_errors(file: str, nn: NN, train_errors: Errors, test_errors: Errors) -> None:
    print(f'saving nn and errors to "{file}"...')
    with open(file, 'wb') as nn_file:
        pickle.dump([nn, train_errors, test_errors], nn_file)


def load_nn_and_errors(file: str) -> Optional[tuple[NN, Errors, Errors]]:
    try:
        with open(file, 'rb') as nn_file:
            print(f'loading nn and errors from "{file}"...')
            return pickle.load(nn_file)
    except FileNotFoundError:
        return None
