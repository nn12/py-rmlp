import numpy as np
from numba import njit


from rmlp.misc import as_row


@njit(cache=True)
def grad_loop(
        error: np.ndarray,
        input: np.ndarray,
        v: np.ndarray,
        w: np.ndarray,
        prev_grad_h: np.ndarray
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    :return: weights gradient, output gradient, error signal
    """
    assert v.shape[0] == w.shape[0]
    assert input.shape[0] == w.shape[1]
    assert prev_grad_h.shape == w.shape

    k: int = w.shape[0]
    n: int = w.shape[1] - w.shape[0] - 1

    dtanh: np.ndarray = as_row(1.0 / np.cosh(v) ** 2).T

    grad_h: np.ndarray = dtanh @ as_row(input)
    grad_h[:, n:n + k] += prev_grad_h[:, n:n + k] * w[:, n:n + k] * dtanh
    assert grad_h.shape == w.shape

    grad_w: np.ndarray = as_row(error).T * grad_h

    error = w.T @ error
    error = error[:n]

    return grad_w, grad_h, error


@njit(cache=True)
def grad(
        error: np.ndarray,
        input: np.ndarray,
        v: np.ndarray,
        w: np.ndarray,
) -> tuple[np.ndarray, np.ndarray]:
    """
    :return: weights gradient, error signal
    """
    assert v.shape[0] == w.shape[0]
    assert input.shape[0] == w.shape[1]

    n: int = w.shape[1] - 1

    dtanh: np.ndarray = as_row(1.0 / np.cosh(v) ** 2).T

    grad_h: np.ndarray = dtanh @ as_row(input)
    assert grad_h.shape == w.shape

    grad_w: np.ndarray = as_row(error).T * grad_h

    error = w.T @ error
    error = error[:n]

    return grad_w, error
