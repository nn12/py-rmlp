import numpy as np
from numba import njit


@njit(cache=True)
def as_row(array: np.ndarray) -> np.ndarray:
    return array.reshape((1, array.shape[0]))
