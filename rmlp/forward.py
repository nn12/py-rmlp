import numpy as np
from numba import njit

from rmlp import NN
from rmlp.types import Outputs, Inputs, OutputsBeforeActivation


# maybe unused
@njit(cache=True)
def sigmoid(z: np.ndarray) -> np.ndarray:
    return 1.0 / (1.0 + np.exp(-z))


@njit(cache=True)
def forward(
        nn: NN,
        data: np.ndarray,
        prev_outputs: Outputs
) -> tuple[Inputs, OutputsBeforeActivation, Outputs]:
    w1, w2, w3 = nn
    h1, h2, h3 = prev_outputs

    in1 = np.hstack((data, np.ones(1), h1))
    v1 = w1 @ in1
    h1 = np.tanh(v1)

    in2 = np.hstack((h1, np.ones(1), h2))
    v2 = w2 @ in2
    h2 = np.tanh(v2)

    in3 = np.hstack((h2, np.ones(1)))
    v3 = w3 @ in3
    h3 = np.tanh(v3)

    return (in1, in2, in3), (v1, v2, v3), (h1, h2, h3)
