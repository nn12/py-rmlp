from typing import Optional

import numpy as np
from numba import njit

from rmlp.test import test, nan_to_zero
from rmlp.forward import forward
from rmlp.nn import NN, load_nn_and_errors, save_nn_and_errors
from rmlp.gradients import grad_loop, grad
from rmlp.types import Errors, Outputs

Inputs: type = tuple[np.ndarray, np.ndarray, np.ndarray]


@njit(cache=True)
def gd(
        nn: NN,
        data: np.ndarray,
        target: np.ndarray,
        learning_rate: float
) -> float:
    w1, w2, w3 = nn
    w1_count, w2_count, w3_count = w1.shape[0], w2.shape[0], w3.shape[0]

    grad_w1, grad_w2, grad_w3 = np.zeros_like(w1), np.zeros_like(w2), np.zeros_like(w3)
    prev_grad_h1, prev_grad_h2 = np.zeros_like(w1), np.zeros_like(w2)
    prev_outputs: Outputs = (np.zeros(w1_count), np.zeros(w2_count), np.zeros(w3_count))

    total_error: float = 0.0
    samples_count: int = data.shape[0]
    learning_rate /= samples_count

    for i in range(samples_count):
        input: np.ndarray = nan_to_zero(data[i])
        (in1, in2, in3), (v1, v2, v3), prev_outputs = forward(nn, input, prev_outputs)

        output: np.ndarray = prev_outputs[-1]
        error: np.ndarray = output - target[i]
        error = nan_to_zero(error)
        total_error += (error ** 2).sum() / 2

        curr_grad_w3, error = grad(error, in3, v3, w3)
        curr_grad_w2, prev_grad_h2, error = grad_loop(error, in2, v2, w2, prev_grad_h2)
        curr_grad_w1, prev_grad_h1, _ = grad_loop(error, in1, v1, w1, prev_grad_h1)

        grad_w1 += curr_grad_w1
        grad_w2 += curr_grad_w2
        grad_w3 += curr_grad_w3

    w1 -= learning_rate * grad_w1
    w2 -= learning_rate * grad_w2
    w3 -= learning_rate * grad_w3

    return total_error / samples_count


cache_is_valid: bool = True


def train(
        nn: NN,
        train_data: np.ndarray,
        train_target: np.ndarray,
        test_data: np.ndarray,
        test_target: np.ndarray,
        learning_rate: float,
        epoch_count: int,
        index: int = 0,
        use_cached: bool = False) -> tuple[NN, Errors, Errors, bool]:
    cache_file: str = f'cache/nn_{index}_{learning_rate}_{epoch_count}.pkl'
    if use_cached:
        cached: Optional[tuple[NN, Errors, Errors]] = load_nn_and_errors(cache_file)
        if cached:
            nn, e1, e2 = cached
            return nn, e1, e2, True

    assert train_data.shape[0] == train_target.shape[0]
    assert test_data.shape[0] == test_target.shape[0]

    train_errors: Errors = np.empty(epoch_count)
    test_errors: Errors = np.empty(epoch_count)

    for i in range(epoch_count):
        train_errors[i] = gd(nn, train_data, train_target, learning_rate)
        _, test_error = test(nn, test_data, test_target)
        test_errors[i] = test_error

    print(f'Average train error: {train_errors.mean():.4f}, average test error: {test_errors.mean():.4f}')

    save_nn_and_errors(cache_file, nn, train_errors, test_errors)

    return nn, train_errors, test_errors, False
