import numpy as np
from numba import njit

from rmlp.forward import forward as fwd
from rmlp.nn import NN
from rmlp.types import Outputs


@njit(cache=True)
def nan_to_zero(array: np.ndarray) -> np.ndarray:
    result: np.ndarray = np.zeros_like(array)
    for i in range(array.shape[0]):
        if not np.isnan(array[i]):
            result[i] = array[i]

    return result


@njit(cache=True)
def forward(nn: NN, data: np.ndarray) -> np.ndarray:
    samples_count: int = data.shape[0]

    w1, w2, w3 = nn
    w1_count, w2_count, w3_count = w1.shape[0], w2.shape[0], w3.shape[0]
    prev_outputs: Outputs = (np.zeros(w1_count), np.zeros(w2_count), np.zeros(w3_count))

    results: np.ndarray = np.empty(shape=(samples_count, w3_count))

    for i in range(samples_count):
        input: np.ndarray = nan_to_zero(data[i])
        _, _, prev_outputs = fwd(nn, input, prev_outputs)
        results[i] = prev_outputs[-1]

    return results


# @njit(cache=True)
def test(
        nn: NN,
        data: np.ndarray,
        target: np.ndarray) -> tuple[np.ndarray, float]:
    assert data.shape[0] == target.shape[0]

    results: np.ndarray = forward(nn, data)
    errors: np.ndarray = (((results - target) ** 2).sum(axis=1) / 2)

    return results, np.nanmean(errors)
