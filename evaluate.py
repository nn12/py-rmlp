import sys

from main import evaluate


def main(args: list[str]) -> None:
    assert len(args) > 1
    if len(args) > 2:
        evaluate(args[1], args[2])
    else:
        evaluate(args[1])


if __name__ == '__main__':
    main(sys.argv)
