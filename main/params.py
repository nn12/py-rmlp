import json
from dataclasses import dataclass, field


@dataclass
class Params:
    clear_caches: bool = field()
    use_saved_nn: bool = field()
    evaluate_only: bool = field()

    use_cached: bool = field()
    data_file: str = field()

    neurons_count: tuple[int, ...] = field()

    train_parameters: list[tuple[float, int]] = field()


TRAIN_PARAMETERS_FILE = 'params.json'


def load_parameters(file: str = TRAIN_PARAMETERS_FILE) -> Params:
    with open(file, 'r') as params_file:
        params: dict = json.load(params_file)

    return Params(
        clear_caches=params.get('clear_caches') or False,
        use_saved_nn=params.get('use_saved_nn') or False,
        evaluate_only=params.get('evaluate_only') or False,
        use_cached=params.get('use_cached') or False,
        data_file=params.get('data_file') or 'data.data',
        neurons_count=params.get('neurons_count') or [1, 1, 1, 1],
        train_parameters=[(it['learning_rate'], it['epoch_count']) for it in params['train_parameters']]
    )
