import glob
import os


def clear_caches():
    files = glob.glob('cache/*')
    for file in files:
        os.remove(file)
