import numpy as np

from main.data import load_data
from main.plot import plot_data
from rmlp import NN, load_nn, test

TRAINED_NN_FILE = 'trained-nn.pkl'


def rmse(x: np.ndarray, y: np.ndarray) -> float:
    return np.sqrt(np.nanmean((x - y) ** 2))


def evaluate(data_file: str, nn_file: str = TRAINED_NN_FILE) -> None:
    nn: NN = load_nn(nn_file)

    train_data, train_target, test_data, test_target = load_data(data_file)

    test_only_outputs, _ = test(nn, test_data, test_target)
    train_outputs, _ = test(nn, train_data, train_target)

    for i in range(test_only_outputs.shape[1]):
        print(f'[Test only] Target column #{i + 1} test RMSE: '
              f'{rmse(test_only_outputs[:, i], test_target[:, i]) * 100:.2f}%')

    plot_data(
        np.array(range(test_data.shape[0])),
        test_only_outputs,
        test_target,
        file=f'graph/{data_file.split("/")[-1]}-{nn_file.split("/")[-1]}-approx-test')

    plot_data(
        np.array(range(train_data.shape[0])),
        train_outputs,
        train_target,
        file=f'graph/{data_file.split("/")[-1]}-{nn_file.split("/")[-1]}-approx-train')
