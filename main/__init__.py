from main.evaluate import evaluate
from main.train import train

__all__ = [
    'evaluate',
    'train'
]
