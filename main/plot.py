import numpy as np
from matplotlib import pyplot

from rmlp import Errors

APPROX_FILE = 'graph/approx'
ERRORS_FILE = 'graph/errors'


def plot_data(
        data: np.ndarray,
        output: np.ndarray,
        target: np.ndarray,
        file: str = APPROX_FILE,
        title: str = None) -> None:
    if len(target.shape) > 1 and target.shape[1] > 1:
        for i in range(target.shape[1]):
            plot_data(data, output[:, i], target[:, i], file=f'{file}-column-{i}', title=f'Column #{i}')
        return

    pyplot.clf()
    with np.printoptions(precision=2, linewidth=1000):
        print(target)
        print(output)

    if any(np.isnan(it) for it in target):
        pyplot.plot(data, target, 'x')
        pyplot.plot(data, output, '.')
    else:
        pyplot.plot(data, target)
        pyplot.plot(data, output)
    pyplot.legend(('Target', 'Output'))
    if title:
        pyplot.title(title)
    pyplot.savefig(f'{file}.png')


def plot_errors(train_errors: Errors, test_errors: Errors) -> None:
    pyplot.clf()

    fig, (ax1, ax2) = pyplot.subplots(ncols=2)

    ax1.plot(train_errors)
    ax1.set_title('Train loss')

    ax2.plot(test_errors)
    ax2.set_title('Test loss')

    pyplot.tight_layout()
    pyplot.savefig(ERRORS_FILE)
