import numpy as np

from main.data import load_data
from main.evaluate import TRAINED_NN_FILE
from main.params import Params, load_parameters
from main.plot import plot_errors
import rmlp
from rmlp import make_nn, NN, Errors, save_nn, load_nn

RAW_NN_FILE = 'nn.pkl'


def train():
    params: Params = load_parameters()

    train_data, train_target, test_data, test_target = load_data(params.data_file)

    nn: NN
    if params.use_saved_nn:
        nn = load_nn(RAW_NN_FILE)
    else:
        n_counts: tuple[int, ...] = tuple(params.neurons_count)
        nn = make_nn(*n_counts)
        save_nn(RAW_NN_FILE, nn)

    all_train_errors: list[Errors] = []
    all_test_errors: list[Errors] = []

    used_cached: bool = True

    for i, (learning_rate, epoch_count) in enumerate(params.train_parameters):
        nn, train_errors, test_errors, used_cached = rmlp.train(
            nn, train_data, train_target, test_data, test_target, learning_rate, epoch_count,
            index=i, use_cached=params.use_cached and used_cached and params.use_saved_nn)

        all_train_errors.append(train_errors)
        all_test_errors.append(test_errors)

    save_nn(TRAINED_NN_FILE, nn)

    train_errors = np.hstack(all_train_errors)
    test_errors = np.hstack(all_test_errors)

    plot_errors(train_errors, test_errors)
