import json

import numpy as np


def normalize(data: np.ndarray) -> np.ndarray:
    assert len(data.shape) == 2

    low: np.ndarray = np.nanmin(data, axis=0)
    high: np.ndarray = np.nanmax(data, axis=0)

    delta: np.ndarray = high - low

    data = data - low
    data = data / delta

    return (data * 1.9) - 0.95


def load_data(file: str) -> tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    with open(file, 'r') as data_file:
        metadata: dict = json.loads(data_file.readline())
        train_size: int = metadata['train_size']
        target_columns_count: int = metadata['target_columns_count']

        data = np.array([[float(it) for it in line.split(',')] for line in data_file.readlines()])

        input, target = data[:, :-target_columns_count], data[:, -target_columns_count:]

    target = normalize(target)

    return input[:train_size], target[:train_size], input[train_size:], target[train_size:]
